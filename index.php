<html>
	<head>
		<title><?php echo "Weather information"; ?></title>
	</head>
	<body>
		<form action = "cityWeather.php" method="post">
			<h3>Enter city name:</h3>
			<div style="position:relative;width:200px;height:25px;border:0;padding:0;margin:0;">
			  <select style="position:absolute;top:0px;left:0px;width:200px; height:25px;line-height:20px;margin:0;padding:0;"
					  onchange="document.getElementById('cityName').value=this.options[this.selectedIndex].text; document.getElementById('idValue').value=this.options[this.selectedIndex].value;">
				<option></option>
				<?php
					$servername = "localhost";
					$username = "root";
					$password = "";
					$dbname = "myDB";
					
					# create connection
					$conn = mysqli_connect($servername, $username, $password);
					# check connection
					if (!$conn) {
					  die("Connection failed: " . mysqli_connect_error());
					}

					# create database (if does not exist)
					$sql = "CREATE DATABASE IF NOT EXISTS myDB";
					if (mysqli_query($conn, $sql)) {
						# do not do anything further
					} else {
						# print error if there is any
						echo "Error creating database: " . mysqli_error($conn);
					}
					mysqli_close($conn);

					# Create connection
					$conn = mysqli_connect($servername, $username, $password, $dbname);
					# Check connection
					if (!$conn) {
					  die("Connection failed: " . mysqli_connect_error());
					}

					$sql = "SELECT name FROM mySearches";
					$result = mysqli_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {
					  # output data of each row
					  while($row = mysqli_fetch_assoc($result)) {
						echo "<option value=" . $row['name'] . ">" . $row['name'] . "</option>";
					  }
					} else {
					  echo "0 results";
					}

					mysqli_close($conn);
				?>
			  </select>
			  <input type="text" name="cityName" id="cityName" 
					 placeholder="add/select a value" onfocus="this.select()"
					 style="position:absolute;top:0px;left:0px;width:183px;width:180px\9;#width:180px;height:23px; height:21px\9;#height:18px;border:1px solid #556;"  >
			  <input name="idValue" id="idValue" type="hidden">
			</div>
			<input type = "submit" name = "submitButton" value = "Search" />
		</form>
	</body>
</html>