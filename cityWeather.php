<?php
	# use the code below to prevent from showing bad request warning in case he city does not exist
	error_reporting(E_ERROR | E_PARSE);
	
	# create database
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "myDB";

	# create connection
	$conn = mysqli_connect($servername, $username, $password);
	# check connection
	if (!$conn) {
	  die("Connection failed: " . mysqli_connect_error());
	}

	# create database (if does not exist)
	$sql = "CREATE DATABASE IF NOT EXISTS myDB";
	if (mysqli_query($conn, $sql)) {
		# do not do anything further
	} else {
		# print error if there is any
		echo "Error creating database: " . mysqli_error($conn);
	}
	mysqli_close($conn);
	
	# create table if it does not exists and enter new value
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	
	$sql = "CREATE TABLE IF NOT EXISTS mySearches (
		name VARCHAR(30) NOT NULL UNIQUE
	)";
	
	if (mysqli_query($conn, $sql)) {
		# do not do anything further
	} else {
		# print error if there is any
		echo "Error creating table: " . mysqli_error($conn);
	}
	
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# use '' around value in order to be able to update successfully
	$sql = "INSERT INTO mySearches ( name ) VALUE ( '" . $_POST["cityName"] . "' )";
	
	if (mysqli_query($conn, $sql)) {
		# do not do anything further
	} else {
		# do nothing
	}

	mysqli_close($conn);
	
	echo "<h3>Weather information for " . $_POST["cityName"] . "</h3>";
	echo "<link rel='stylesheet' type='text/css' href='customCSS.css' />";
	# add the entered city
	$url = "http://api.weatherapi.com/v1/current.json?key=510eb7be47904818b8a174646202809&q=" . $_POST["cityName"];

	# make the request
	$json = file_get_contents($url);
	
	# use true to return json as array
	$data=json_decode($json, true);
	
	# check to see if result is fine
	if ($data === null) {
		echo "Sorry, could not find the city you are looking for...";
	} else {
		echo "<table>
				<tr>
					<th>Category</th>
					<th>Information</th>
				</tr>
				<tr>
					<td>Name</td>
					<td>" . $data["location"]["name"] . "</td>
				</tr>
				<tr>
					<td>Region</td>
					<td>" . $data["location"]["region"] . "</td>
				</tr>
				<tr>
					<td>Country</td>
					<td>" . $data["location"]["country"] . "</td>
				</tr>
				<tr>
					<td>Local Time</td>
					<td>" . $data["location"]["localtime"] . "</td>
				</tr>
				<tr>
					<td>Current Temperature (in Celcius)</td>
					<td>" . $data["current"]["temp_c"] . "&#8451</td>
				</tr>
				<tr>
					<td>Weather Condition</td>
					<td>" . $data["current"]["condition"]["text"] . "</td>
				</tr>
			</table>";
	}
?>